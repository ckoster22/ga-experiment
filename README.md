# Disclaimer

* This is not production quality code.
* This is I-ain't-got-time-to-refactor quality code. Try to ignore things I'm doing terribly.
* I'm not responsible for your CPU overheating.
* Deal with it.

# Preparation

1. Have a webcam
2. Plug in your laptop

# Running locally

`npm install`

`npm start`

Navigate to http://localhost:8080/

Open the console to track progress. Click the snapshot button. Wait 30-90 minutes. It'll stop after 30,000 generations or when newly generated circles are smaller than 1 pixel.

When it's done click Play Movie at the bottom.

Fun fact. Take a few steps away from the screen and the image looks even better.

# Misc

This Genetic Algorithm is a highly modified variant of a typical vanilla GA. I've added a few circles-on-a-canvas based enhancements so that this can run in less than 2 hours rather than running all day.
