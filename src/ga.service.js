module.exports = function() {
    var individualsWithScores = [],
        best,
        population,
        generationSize = 20,
        crossOverAndMutate,
        determineScore;

    // poor man's clone
    function clone(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    function runStep() {
        individualsWithScores = [];

        // console.log('scoring individuals..');
        for (var i = 0; i < this.population.length; ++i) {
            var individual = this.population[i];

            individualsWithScores.push({
                individual: individual,
                score: getScoreForIndividual(individual, this)
            });
        }

        // console.log('killing off unsuccessful individuals..');
        killWorst50Percent();

        // console.log('generating the next generation..');
        this.population = createNewPopulation(this);
        // console.log('best score=' + individualsWithScores[0].score + ', # shapes=' + individualsWithScores[0].individual.circles.length);

        return getBestScoringIndividual();
    }

    function getScoreForIndividual(individual, context) {
        return context.determineScore(individual);
    }

    function killWorst50Percent() {
        individualsWithScores.sort(function(first, second) {
            return first.score - second.score;
        });

        individualsWithScores.splice(individualsWithScores.length / 2);
    }

    function getBestScoringIndividual() {
        return individualsWithScores[0];
    }

    function createNewPopulation(context) {
        var newPopulation = [];

        // breed two random parents
        var copyOfIndividualsWithScores = clone(individualsWithScores);

        copyOfIndividualsWithScores.sort(function() {
            return Math.random() < 0.5;
        });

        // generate 3 offspring by crossing over and mutating
        // add the best parent back into the pool
        for (var i = 0; i < copyOfIndividualsWithScores.length; i += 2) {
            var parent1 = copyOfIndividualsWithScores[i];
            var parent2 = copyOfIndividualsWithScores[i+1];

            newPopulation.push(context.crossOverAndMutate(clone(parent1.individual), clone(parent2.individual)));
            newPopulation.push(context.crossOverAndMutate(clone(parent1.individual), clone(parent2.individual)));
            newPopulation.push(context.crossOverAndMutate(clone(parent1.individual), clone(parent2.individual)));

            // Keep the best parent from the pair so we never get worse
            if (parent1.score < parent2.score) {
                newPopulation.push(parent1.individual);
            } else {
                newPopulation.push(parent2.individual);
            }
        }

        return newPopulation;
    }

    function getFinalSolution() {
        return getBestScoringIndividual().individual;
    }

    return {
        population: population,
        generationSize: generationSize,
        crossOverAndMutate: crossOverAndMutate,
        determineScore: determineScore,
        runStep: runStep,
        getFinalSolution: getFinalSolution
    };
}
