var $ = require('jquery')
var webcamCaputurer = require('./webcam.capture.js');
var gaService = require('./ga.service.js')();

var mutationRate = 0.03;
var crossOverRate = 0.3;
var generationSize = 12;
var width = 640;
var height = 480;
var currentPercentage = 1;
var generation = 0;
var currentScore = 0;
var timesAtThisScore;
var radius = 200;
var newCircleThresh = 0.999;
var numInitialCircles = 5;
var gaCanvas = $('#gaCanvas')[0];
var images = [];
var currentImageIndex = 0;
var imageData = {};

$('#playMovieButton').on('click', playMoveClick)

webcamCaputurer(function(newValue) {
    imageData = newValue;
    if (newValue) {
        runGA();
    }
});

function runGA() {
    gaService.population = generateInitialPopulation();
    gaService.generationSize = generationSize;
    gaService.crossOverAndMutate = crossOverAndMutate;
    gaService.determineScore = determineScore;

    runBatch();

    var finalSolution = gaService.getFinalSolution();
    drawIndividual(finalSolution);
}

function runBatch() {
    for (var i = 0; i < 20; ++i) {
        var currentBestIndividual = gaService.runStep();
        // var score = determineScore(currentBestIndividual);
        var score = currentBestIndividual.score;

        if (score == currentScore) {
            timesAtThisScore++;

            if (timesAtThisScore == 7) {
                if (radius > 1) {
                    radius *= 0.98;
                } else {
                    radius = 1;
                }

                newCircleThresh *= 0.995;
                timesAtThisScore = 0;
            }
        } else {
            timesAtThisScore = 0;
        }

        currentScore = score;
        // display this generation's best solution
        currentPercentage = currentScore / 178000000;
        console.log(currentPercentage + ' ' + generation++);

        if (generation % 5 == 0) {
            images.push(gaCanvas.toDataURL());
        }

        drawIndividual(currentBestIndividual.individual);
        console.log(timesAtThisScore + ' ' + radius + ' ' + newCircleThresh);
    }

    if (generation+20 < 20000 && Math.floor(radius) !== 1) {
        // Release control to let the browser re-render the canvas
        setTimeout(function() {
            runBatch();
        }, 0);
    } else {
        var img = $('#imagePlayback')[0];
        img.src = images[0];
    }
}

function playMoveClick() {
    console.log('playing..');
    currentImageIndex = 0;
    drawImages();
};

function drawImages() {
    var img = $('#imagePlayback')[0];
    img.src = images[currentImageIndex++];

    if (currentImageIndex < images.length) {
        setTimeout(function() {
            drawImages();
        }, 2);
    } else {
        console.log('done');
    }
}

function generateInitialPopulation() {
    var population = [];
    var circles;

    for (var i = 0; i < generationSize; ++i) {
        circles = [];

        for (var j = 0; j < numInitialCircles; ++j) {
            circles.push(generateRandomCircle());
        }

        population.push({
            circles: circles
        })
    }

    return population;
}

function generateRandomCircle() {
    return {
        x: Math.floor(Math.random() * width),
        y: Math.floor(Math.random() * height),
        radius: Math.floor(radius),
        red: Math.floor(Math.random() * 255),
        green: Math.floor(Math.random() * 255),
        blue: Math.floor(Math.random() * 255),
        alpha: Math.random().toFixed(2)
    };
}

function determineScore(individual) {
    drawIndividual(individual);

    var context = gaCanvas.getContext('2d');
    var pix = context.getImageData(0, 0, width, height).data;
    var red, green, blue, alpha;
    var original = imageData.data;
    var cost = 0;

    for (var i = 0, n = pix.length; i < n; i += 4) {
        red = pix[i];
        green = pix[i+1];
        blue = pix[i+2];
        alpha = pix[i+3];

        cost += Math.abs(red - original[i]);
        cost += Math.abs(green - original[i+1]);
        cost += Math.abs(blue - original[i+2]);
        cost += Math.abs(alpha - original[i+3]);
    }

    return cost;
}

function drawIndividual(individual) {
    var context = gaCanvas.getContext('2d');

    context.clearRect(0, 0, width, height);

    for (var i = 0; i < individual.circles.length; ++i) {
        var circle = individual.circles[i];
        context.beginPath();
        context.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
        context.fillStyle = 'rgba(' + circle.red + ', ' + circle.green + ', ' + circle.blue + ', ' + circle.alpha + ')';
        context.fill();
    }
}

function crossOverAndMutate(parent1, parent2) {
    var firstParent, secondParent;
    var childCircles;

    if (Math.random() < 0.5) {
        firstParent = parent1;
        secondParent = parent2;
    } else {
        firstParent = parent2;
        secondParent = parent1;
    }

    // Copy 30% from one parent and 70% from the other
    childCircles = crossOver(firstParent, secondParent);

    // Maybe add a new circle
    if (Math.random() > newCircleThresh) {
        childCircles.push(generateRandomCircle());
    } else {
        // Mutate some of the attributes
        mutate(childCircles);
    }

    return {
        circles: childCircles
    };
}

function crossOver(firstParent, secondParent) {
    var childCircles = firstParent.circles.slice(0, Math.floor(crossOverRate * firstParent.circles.length));
    childCircles = childCircles.concat(secondParent.circles.slice(Math.floor(crossOverRate * secondParent.circles.length)));

    return childCircles;
}

function mutate(childCircles) {
    var currentCircle = childCircles[Math.floor(Math.random() * childCircles.length)];
    var randAttr = Math.floor(Math.random() * 7);
    var lowerX = Math.max(0, currentCircle.x - currentCircle.x * mutationRate);
    var upperX = Math.min(width, currentCircle.x + currentCircle.x * mutationRate);
    var lowerY = Math.max(0, currentCircle.y - currentCircle.y * mutationRate);
    var upperY = Math.min(height, currentCircle.y + currentCircle.y * mutationRate);
    var lowerRadius = Math.max(0, currentCircle.radius - currentCircle.radius * mutationRate);
    var upperRadius = Math.min(300, currentCircle.radius + currentCircle.radius * mutationRate);
    var lowerRed = Math.max(0, currentCircle.red - currentCircle.red * mutationRate);
    var upperRed = Math.min(255, currentCircle.red + currentCircle.red * mutationRate);
    var lowerGreen = Math.max(0, currentCircle.green - currentCircle.green * mutationRate);
    var upperGreen = Math.min(255, currentCircle.green + currentCircle.green * mutationRate);
    var lowerBlue = Math.max(0, currentCircle.blue - currentCircle.blue * mutationRate);
    var upperBlue = Math.min(255, currentCircle.blue + currentCircle.blue * mutationRate);

    switch (randAttr) {
        case 0:
            currentCircle.x = mutateValue(currentCircle.x, lowerX, upperX);
            break;
        case 1:
            currentCircle.y = mutateValue(currentCircle.y, lowerY, upperY);
            break;
        case 2:
            currentCircle.radius = mutateValue(currentCircle.radius, lowerRadius, upperRadius);
            break;
        case 3:
            currentCircle.red = Math.round(mutateValue(currentCircle.red, lowerRed, upperRed));
            break;
        case 4:
            currentCircle.green = Math.round(mutateValue(currentCircle.green, lowerGreen, upperGreen));
            break;
        case 5:
            currentCircle.blue = Math.round(mutateValue(currentCircle.blue, lowerBlue, upperBlue));
            break;
        case 6:
            currentCircle.alpha = mutateValue(currentCircle.alpha, 0, 1).toFixed(2);
            break;
    }
}

function mutateValue(current, min, max) {
    current = parseFloat(current);
    var range = max - min;
    var mutateAmount = Math.random() * range;
    var newVal;

    if (current + mutateAmount > max && current) {
        newVal = current - mutateAmount;
    } else if (current - mutateAmount < min) {
        newVal = current + mutateAmount;
    } else if (Math.random() < 0.5) {
        newVal = current - mutateAmount;
    } else {
        newVal = current + mutateAmount;
    }

    if (newVal > max) {
        newVal = max;
    } else if (newVal < min) {
        newVal = min;
    }

    return newVal;
}
