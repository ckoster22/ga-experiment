var $ = require('jquery');

module.exports = function(setImageCb) {
    var canvas = $('#screenshot-canvas')[0];
    var ctx = canvas.getContext('2d');
    var localMediaStream;
    var errorCallback;

    $('#stopButton').on('click', stop);
    $('#captureButton').on('click', takeSnapshot);

    function errorCallback(error) {
        console.log('There was a problem with using your webcam! %o', error);
    };

    function takeSnapshot() {
        if (localMediaStream) {
            snapshot();
        } else {
            if (navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({audio: false, video: true}).then(function(stream) {
                    document.querySelector('#screenshot-stream').src = window.URL.createObjectURL(stream);
                    localMediaStream = window.URL.createObjectURL(stream);
                    sizeCanvas();
                }).catch(errorCallback);
            } else if (navigator.webkitGetUserMedia) {
                navigator.webkitGetUserMedia({video: true}, function(stream) {
                    document.querySelector('#screenshot-stream').src = window.URL.createObjectURL(stream);
                    localMediaStream = stream;
                    sizeCanvas();
                }, errorCallback);
            }
        }
    };

    function stop() {
        $('#screenshot-stream')[0].pause();
        localMediaStream.stop(); // Doesn't do anything in Chrome
        $('#screenshot-stream').hide();
    };

    function sizeCanvas() {
        // video.onloadedmetadata not firing in Chrome so we have to hack.
        // See crbug.com/110938.
        setTimeout(function() {
            canvas.width = $('#screenshot-stream')[0].videoWidth;
            canvas.height = $('#screenshot-stream')[0].videoHeight;
            console.log('OK now click the capture button');
        }, 2000); // stupid shit
    }

    function snapshot() {
        ctx.drawImage($('#screenshot-stream')[0], 0, 0);
        setImageCb(ctx.getImageData(0, 0, canvas.width, canvas.height));
        stop();
    }
}
